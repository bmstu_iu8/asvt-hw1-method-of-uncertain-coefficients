package dev.cornflower.asvtHw1.repository;

import dev.cornflower.asvtHw1.entity.Coefficient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CoefRepository extends JpaRepository<Coefficient, Integer> {
    List<Coefficient> findAllByF(String f);
}
