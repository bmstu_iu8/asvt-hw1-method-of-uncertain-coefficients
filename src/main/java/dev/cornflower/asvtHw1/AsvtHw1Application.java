package dev.cornflower.asvtHw1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AsvtHw1Application {

	public static void main(String[] args) {
		SpringApplication.run(AsvtHw1Application.class, args);
	}

}
