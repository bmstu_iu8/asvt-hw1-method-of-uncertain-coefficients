package dev.cornflower.asvtHw1.entity;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "koef")
public class Coefficient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String x6;
    private String x5;
    private String x4;
    private String X3;
    private String X2;
    private String X1;

    private String X21;
    private String X31;
    private String X41;
    private String X51;
    private String X61;
    private String X32;
    private String X42;
    private String X52;
    private String X62;
    private String X43;
    private String X53;
    private String X63;
    private String X54;
    private String X64;
    private String X65;

    private String X321;
    private String X421;
    private String X521;
    private String X621;
    private String X431;
    private String X531;
    private String X631;
    private String X541;

    private String X641;

    private String X651;

    private String X432;

    private String X532;

    private String X632;

    private String X542;

    private String X642;

    private String X652;

    private String X543;

    private String X643;

    private String X653;

    private String X654;

    private String X4321;

    private String X5321;

    private String X6321;

    private String X5421;

    private String X6421;

    private String X6521;

    private String X5431;

    private String X6431;

    private String X6531;

    private String X5432;

    private String X6432;

    private String X6532;

    private String X6542;

    private String X6543;

    private String X54321;

    private String X64321;

    private String X65321;

    private String X65421;

    private String X65432;

    private String X654321;

    private String f;
}
