package dev.cornflower.asvtHw1.service;

import dev.cornflower.asvtHw1.entity.Coefficient;
import dev.cornflower.asvtHw1.repository.CoefRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SecondStageService {
    private final CoefRepository coefRepository;
    public void startSecondStage() {
        List<Coefficient> all = coefRepository.findAll();
        List<Coefficient> nullableCoef = coefRepository.findAllByF("0");
        nullableCoef.forEach(nil -> all.forEach(c -> secondStageCheck(nil, c)));
    }

    private void secondStageCheck(Coefficient nil, Coefficient every) {
        if (!StringUtils.isEmpty(nil.getX21()) && every.getX21().equals(nil.getX21())) {
            every.setX21("");
        }

        if (!StringUtils.isEmpty(nil.getX31()) && every.getX31().equals(nil.getX31())) {
            every.setX31("");
        }

        if (!StringUtils.isEmpty(nil.getX41()) && every.getX41().equals(nil.getX41())) {
            every.setX41("");
        }

        if (!StringUtils.isEmpty(nil.getX51()) && every.getX51().equals(nil.getX51())) {
            every.setX51("");
        }

        if (!StringUtils.isEmpty(nil.getX61()) && every.getX61().equals(nil.getX61())) {
            every.setX61("");
        }

        if (!StringUtils.isEmpty(nil.getX32()) && every.getX32().equals(nil.getX32())) {
            every.setX32("");
        }

        if (!StringUtils.isEmpty(nil.getX42()) && every.getX42().equals(nil.getX42())) {
            every.setX42("");
        }

        if (!StringUtils.isEmpty(nil.getX52()) && every.getX52().equals(nil.getX52())) {
            every.setX52("");
        }

        if (!StringUtils.isEmpty(nil.getX62()) && every.getX62().equals(nil.getX62())) {
            every.setX62("");
        }

        if (!StringUtils.isEmpty(nil.getX43()) && every.getX43().equals(nil.getX43())) {
            every.setX43("");
        }

        if (!StringUtils.isEmpty(nil.getX53()) && every.getX53().equals(nil.getX53())) {
            every.setX53("");
        }

        if (!StringUtils.isEmpty(nil.getX63()) && every.getX63().equals(nil.getX63())) {
            every.setX63("");
        }

        if (!StringUtils.isEmpty(nil.getX54()) && every.getX54().equals(nil.getX54())) {
            every.setX54("");
        }

        if (!StringUtils.isEmpty(nil.getX64()) && every.getX64().equals(nil.getX64())) {
            every.setX64("");
        }

        if (!StringUtils.isEmpty(nil.getX65()) && every.getX65().equals(nil.getX65())) {
            every.setX65("");
        }

        if (!StringUtils.isEmpty(nil.getX321()) && every.getX321().equals(nil.getX321())) {
            every.setX321("");
        }

        if (!StringUtils.isEmpty(nil.getX421()) && every.getX421().equals(nil.getX421())) {
            every.setX421("");
        }

        if (!StringUtils.isEmpty(nil.getX521()) && every.getX521().equals(nil.getX521())) {
            every.setX521("");
        }

        if (!StringUtils.isEmpty(nil.getX621()) && every.getX621().equals(nil.getX621())) {
            every.setX621("");
        }

        if (!StringUtils.isEmpty(nil.getX431()) && every.getX431().equals(nil.getX431())) {
            every.setX431("");
        }

        if (!StringUtils.isEmpty(nil.getX531()) && every.getX531().equals(nil.getX531())) {
            every.setX531("");
        }

        if (!StringUtils.isEmpty(nil.getX631()) && every.getX631().equals(nil.getX631())) {
            every.setX631("");
        }

        if (!StringUtils.isEmpty(nil.getX541()) && every.getX541().equals(nil.getX541())) {
            every.setX541("");
        }

        if (!StringUtils.isEmpty(nil.getX641()) && every.getX641().equals(nil.getX641())) {
            every.setX641("");
        }

        if (!StringUtils.isEmpty(nil.getX651()) && every.getX651().equals(nil.getX651())) {
            every.setX651("");
        }

        if (!StringUtils.isEmpty(nil.getX432()) && every.getX432().equals(nil.getX432())) {
            every.setX432("");
        }

        if (!StringUtils.isEmpty(nil.getX532()) && every.getX532().equals(nil.getX532())) {
            every.setX532("");
        }

        if (!StringUtils.isEmpty(nil.getX632()) && every.getX632().equals(nil.getX632())) {
            every.setX632("");
        }


        if (!StringUtils.isEmpty(nil.getX542()) && every.getX542().equals(nil.getX542())) {
            every.setX542("");
        }

        if (!StringUtils.isEmpty(nil.getX642()) && every.getX642().equals(nil.getX642())) {
            every.setX642("");
        }

        if (!StringUtils.isEmpty(nil.getX652()) && every.getX652().equals(nil.getX652())) {
            every.setX652("");
        }

        if (!StringUtils.isEmpty(nil.getX543()) && every.getX543().equals(nil.getX543())) {
            every.setX543("");
        }

        if (!StringUtils.isEmpty(nil.getX643()) && every.getX643().equals(nil.getX643())) {
            every.setX643("");
        }

        if (!StringUtils.isEmpty(nil.getX653()) && every.getX653().equals(nil.getX653())) {
            every.setX653("");
        }

        if (!StringUtils.isEmpty(nil.getX654()) && every.getX654().equals(nil.getX654())) {
            every.setX654("");
        }

        if (!StringUtils.isEmpty(nil.getX4321()) && every.getX4321().equals(nil.getX4321())) {
            every.setX4321("");
        }

        if (!StringUtils.isEmpty(nil.getX5321()) && every.getX5321().equals(nil.getX5321())) {
            every.setX5321("");
        }

        if (!StringUtils.isEmpty(nil.getX6321()) && every.getX6321().equals(nil.getX6321())) {
            every.setX6321("");
        }

        if (!StringUtils.isEmpty(nil.getX5421()) && every.getX5421().equals(nil.getX5421())) {
            every.setX5421("");
        }


        if (!StringUtils.isEmpty(nil.getX6421()) && every.getX6421().equals(nil.getX6421())) {
            every.setX6421("");
        }

        if (!StringUtils.isEmpty(nil.getX6521()) && every.getX6521().equals(nil.getX6521())) {
            every.setX6521("");
        }

        if (!StringUtils.isEmpty(nil.getX5431()) && every.getX5431().equals(nil.getX5431())) {
            every.setX5431("");
        }

        if (!StringUtils.isEmpty(nil.getX6431()) && every.getX6431().equals(nil.getX6431())) {
            every.setX6431("");
        }

        if (!StringUtils.isEmpty(nil.getX6531()) && every.getX6531().equals(nil.getX6531())) {
            every.setX6531("");
        }

        if (!StringUtils.isEmpty(nil.getX5432()) && every.getX5432().equals(nil.getX5432())) {
            every.setX5432("");
        }

        if (!StringUtils.isEmpty(nil.getX6432()) && every.getX6432().equals(nil.getX6432())) {
            every.setX6432("");
        }

        if (!StringUtils.isEmpty(nil.getX6532()) && every.getX6532().equals(nil.getX6532())) {
            every.setX6532("");
        }

        if (!StringUtils.isEmpty(nil.getX6542()) && every.getX6542().equals(nil.getX6542())) {
            every.setX6542("");
        }

        if (!StringUtils.isEmpty(nil.getX6542()) && every.getX6542().equals(nil.getX6542())) {
            every.setX6542("");
        }

        if (!StringUtils.isEmpty(nil.getX6543()) && every.getX6543().equals(nil.getX6543())) {
            every.setX6543("");
        }

        if (!StringUtils.isEmpty(nil.getX54321()) && every.getX54321().equals(nil.getX54321())) {
            every.setX54321("");
        }

        if (!StringUtils.isEmpty(nil.getX64321()) && every.getX64321().equals(nil.getX64321())) {
            every.setX64321("");
        }

        if (!StringUtils.isEmpty(nil.getX65321()) && every.getX65321().equals(nil.getX65321())) {
            every.setX65321("");
        }

        if (!StringUtils.isEmpty(nil.getX65421()) && every.getX65421().equals(nil.getX65421())) {
            every.setX65421("");
        }

        if (!StringUtils.isEmpty(nil.getX65432()) && every.getX65432().equals(nil.getX65432())) {
            every.setX65432("");
        }

        if (!StringUtils.isEmpty(nil.getX654321()) && every.getX654321().equals(nil.getX654321())) {
            every.setX654321("");
        }
        coefRepository.save(every);
    }

}
