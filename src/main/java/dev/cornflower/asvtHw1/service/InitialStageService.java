package dev.cornflower.asvtHw1.service;

import dev.cornflower.asvtHw1.entity.Coefficient;
import dev.cornflower.asvtHw1.repository.CoefRepository;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class InitialStageService {
    private final CoefRepository coefRepository;
    private final SecondStageService secondStageService;
    private final ThirdStageService thirdStageService;
    private static final Integer[] f = new Integer[]{1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0,
            1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 1};

    @PostConstruct
    public void coefProcessing() {
        for (int i = 0; i < 64; i++) {
            String[] binaryNumbers = String.format("%6s",
                            Integer.toBinaryString(i))
                    .replace(' ', '0').split("");
            Coefficient row = new Coefficient();
            row.setId(i + 1);
            fillDoubleConst(row, binaryNumbers);
            fillTripleConst(row, binaryNumbers);
            fillQuarterConst(row, binaryNumbers);
            fillFivefold(row, binaryNumbers);

            row.setX654321(String.join("", binaryNumbers[5], binaryNumbers[4],
                    binaryNumbers[3], binaryNumbers[2], binaryNumbers[1], binaryNumbers[0]));
            row.setF(Integer.toString(f[i]));
            coefRepository.save(row);
        }

        secondStageService.startSecondStage();
        thirdStageService.startThirdStage();
    }

    private void fillDoubleConst(Coefficient row, String[] binaryNumbers) {
        row.setX6(binaryNumbers[5]);
        row.setX5(binaryNumbers[4]);
        row.setX4(binaryNumbers[3]);
        row.setX3(binaryNumbers[2]);
        row.setX2(binaryNumbers[1]);
        row.setX1(binaryNumbers[0]);

        row.setX21(String.join("", binaryNumbers[1], binaryNumbers[0]));
        row.setX31(String.join("", binaryNumbers[2], binaryNumbers[0]));
        row.setX41(String.join("", binaryNumbers[3], binaryNumbers[0]));
        row.setX51(String.join("", binaryNumbers[4], binaryNumbers[0]));
        row.setX61(String.join("", binaryNumbers[5], binaryNumbers[0]));

        row.setX32(String.join("", binaryNumbers[2], binaryNumbers[1]));
        row.setX42(String.join("", binaryNumbers[3], binaryNumbers[1]));
        row.setX52(String.join("", binaryNumbers[4], binaryNumbers[1]));
        row.setX62(String.join("", binaryNumbers[5], binaryNumbers[1]));

        row.setX43(String.join("", binaryNumbers[3], binaryNumbers[2]));
        row.setX53(String.join("", binaryNumbers[4], binaryNumbers[2]));
        row.setX63(String.join("", binaryNumbers[5], binaryNumbers[2]));
        row.setX64(String.join("", binaryNumbers[5], binaryNumbers[3]));

        row.setX54(String.join("", binaryNumbers[4], binaryNumbers[3]));
        row.setX65(String.join("", binaryNumbers[5], binaryNumbers[3]));
        row.setX65(String.join("", binaryNumbers[5], binaryNumbers[5]));
    }

    private void fillTripleConst(Coefficient row, String[] binaryNumbers) {
        row.setX321(String.join("", binaryNumbers[2], binaryNumbers[1], binaryNumbers[0]));
        row.setX421(String.join("", binaryNumbers[3], binaryNumbers[1], binaryNumbers[0]));
        row.setX521(String.join("", binaryNumbers[4], binaryNumbers[1], binaryNumbers[0]));
        row.setX621(String.join("", binaryNumbers[5], binaryNumbers[1], binaryNumbers[0]));

        row.setX431(String.join("", binaryNumbers[3], binaryNumbers[2], binaryNumbers[0]));
        row.setX531(String.join("", binaryNumbers[4], binaryNumbers[2], binaryNumbers[0]));
        row.setX631(String.join("", binaryNumbers[5], binaryNumbers[2], binaryNumbers[0]));

        row.setX541(String.join("", binaryNumbers[4], binaryNumbers[3], binaryNumbers[0]));
        row.setX641(String.join("", binaryNumbers[5], binaryNumbers[3], binaryNumbers[0]));

        row.setX651(String.join("", binaryNumbers[5], binaryNumbers[4], binaryNumbers[0]));

        row.setX432(String.join("", binaryNumbers[3], binaryNumbers[2], binaryNumbers[1]));
        row.setX532(String.join("", binaryNumbers[4], binaryNumbers[2], binaryNumbers[1]));
        row.setX632(String.join("", binaryNumbers[5], binaryNumbers[2], binaryNumbers[1]));

        row.setX542(String.join("", binaryNumbers[4], binaryNumbers[3], binaryNumbers[1]));
        row.setX642(String.join("", binaryNumbers[5], binaryNumbers[3], binaryNumbers[1]));

        row.setX652(String.join("", binaryNumbers[5], binaryNumbers[4], binaryNumbers[1]));
        row.setX543(String.join("", binaryNumbers[4], binaryNumbers[3], binaryNumbers[2]));
        row.setX643(String.join("", binaryNumbers[5], binaryNumbers[3], binaryNumbers[2]));
        row.setX653(String.join("", binaryNumbers[5], binaryNumbers[4], binaryNumbers[2]));
        row.setX654(String.join("", binaryNumbers[5], binaryNumbers[4], binaryNumbers[3]));
    }

    private void fillQuarterConst(Coefficient row, String[] binaryNumbers) {
        row.setX4321(String.join("", binaryNumbers[3], binaryNumbers[2],
                binaryNumbers[1], binaryNumbers[0]));
        row.setX5321(String.join("", binaryNumbers[4], binaryNumbers[2],
                binaryNumbers[1], binaryNumbers[0]));
        row.setX6321(String.join("", binaryNumbers[5], binaryNumbers[2],
                binaryNumbers[1], binaryNumbers[0]));

        row.setX5421(String.join("", binaryNumbers[4], binaryNumbers[3],
                binaryNumbers[1], binaryNumbers[0]));
        row.setX6421(String.join("", binaryNumbers[5], binaryNumbers[3],
                binaryNumbers[1], binaryNumbers[0]));

        row.setX6521(String.join("", binaryNumbers[5], binaryNumbers[4],
                binaryNumbers[1], binaryNumbers[0]));

        row.setX5431(String.join("", binaryNumbers[4], binaryNumbers[3],
                binaryNumbers[2], binaryNumbers[0]));
        row.setX6431(String.join("", binaryNumbers[5], binaryNumbers[3],
                binaryNumbers[2], binaryNumbers[0]));
        row.setX6531(String.join("", binaryNumbers[5], binaryNumbers[4],
                binaryNumbers[2], binaryNumbers[0]));

        row.setX5432(String.join("", binaryNumbers[4], binaryNumbers[3],
                binaryNumbers[2], binaryNumbers[1]));
        row.setX6432(String.join("", binaryNumbers[5], binaryNumbers[3],
                binaryNumbers[2], binaryNumbers[1]));

        row.setX6532(String.join("", binaryNumbers[5], binaryNumbers[4],
                binaryNumbers[2], binaryNumbers[1]));

        row.setX6542(String.join("", binaryNumbers[5], binaryNumbers[4],
                binaryNumbers[3], binaryNumbers[1]));
        row.setX6543(String.join("", binaryNumbers[5], binaryNumbers[4],
                binaryNumbers[3], binaryNumbers[2]));
    }

    private void fillFivefold(Coefficient row, String[] binaryNumbers) {
        row.setX54321(String.join("", binaryNumbers[4], binaryNumbers[3],
                binaryNumbers[2], binaryNumbers[1], binaryNumbers[0]));
        row.setX64321(String.join("", binaryNumbers[5], binaryNumbers[3],
                binaryNumbers[2], binaryNumbers[1], binaryNumbers[0]));

        row.setX65321(String.join("", binaryNumbers[5], binaryNumbers[4],
                binaryNumbers[2], binaryNumbers[1], binaryNumbers[0]));
        row.setX65421(String.join("", binaryNumbers[5], binaryNumbers[4],
                binaryNumbers[3], binaryNumbers[1], binaryNumbers[0]));

        row.setX65432(String.join("", binaryNumbers[5], binaryNumbers[4],
                binaryNumbers[3], binaryNumbers[2], binaryNumbers[1]));
    }
}
