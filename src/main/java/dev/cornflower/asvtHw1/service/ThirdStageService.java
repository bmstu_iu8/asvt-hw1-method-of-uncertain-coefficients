package dev.cornflower.asvtHw1.service;

import dev.cornflower.asvtHw1.entity.Coefficient;
import dev.cornflower.asvtHw1.repository.CoefRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ThirdStageService {
    private final CoefRepository coefRepository;

    //todo: написать проверки для всех конституент
    public void startThirdStage() {
        List<Coefficient> all = coefRepository.findAll();
        all.forEach(c -> {
            validateX21(c);
            validateX31(c);
            validateX41(c);
            validateX321(c);
            validateX631(c);
            validateX632(c);
            validateX4321(c);
            validateX5321(c);
            validateX6321(c);
            validateX6521(c);
            validateX5431(c);
            validateX5432(c);
            validateX6432(c);
            coefRepository.save(c);
        });
    }

    private void validateX21(Coefficient row) {
        if (StringUtils.isEmpty(row.getX21())) {
            return;
        }
        String[] x21Vector = row.getX21().split("");
        String x1_f = x21Vector[1];
        String x2_f = x21Vector[0];

        if (!StringUtils.isEmpty(row.getX321())) {

            String[] x321Vector = row.getX321().split("");
            String x1_s = x321Vector[2];
            String x2_s = x321Vector[1];

            if (x1_f.equals(x1_s) && x2_f.equals(x2_s)) {
                row.setX321("");
            }

        }
        if (!StringUtils.isEmpty(row.getX421())) {
            String[] x421Vector = row.getX421().split("");
            String x1_s = x421Vector[2];
            String x2_s = x421Vector[1];

            if (x1_f.equals(x1_s) && x2_f.equals(x2_s)) {
                row.setX421("");
            }

        }
        if (!StringUtils.isEmpty(row.getX521())) {

            String[] x521Vector = row.getX521().split("");
            String x1_s = x521Vector[2];
            String x2_s = x521Vector[1];

            if (x1_f.equals(x1_s) && x2_f.equals(x2_s)) {
                row.setX521("");
            }
        }

        if (!StringUtils.isEmpty(row.getX621())) {

            String[] x621Vector = row.getX621().split("");
            String x1_s = x621Vector[2];
            String x2_s = x621Vector[1];

            if (x1_f.equals(x1_s) && x2_f.equals(x2_s)) {
                row.setX621("");
            }

        }

        if (!StringUtils.isEmpty(row.getX4321())) {

            String[] x4321Vector = row.getX4321().split("");
            String x1_s = x4321Vector[3];
            String x2_s = x4321Vector[2];

            if (x1_f.equals(x1_s) && x2_f.equals(x2_s)) {
                row.setX4321("");
            }

        }

        if (!StringUtils.isEmpty(row.getX5321())) {

            String[] x5321Vector = row.getX5321().split("");
            String x1_s = x5321Vector[3];
            String x2_s = x5321Vector[2];

            if (x1_f.equals(x1_s) && x2_f.equals(x2_s)) {
                row.setX5321("");
            }
        }

        if (!StringUtils.isEmpty(row.getX6321())) {
            String[] x6321Vector = row.getX6321().split("");
            String x1_s = x6321Vector[3];
            String x2_s = x6321Vector[2];

            if (x1_f.equals(x1_s) && x2_f.equals(x2_s)) {
                row.setX6321("");
            }
        }

        if (!StringUtils.isEmpty(row.getX5421())) {
            String[] x5421Vector = row.getX5421().split("");
            String x1_s = x5421Vector[3];
            String x2_s = x5421Vector[2];

            if (x1_f.equals(x1_s) && x2_f.equals(x2_s)) {
                row.setX5421("");
            }
        }

        if (!StringUtils.isEmpty(row.getX6421())) {
            String[] x6421Vector = row.getX6421().split("");
            String x1_s = x6421Vector[3];
            String x2_s = x6421Vector[2];

            if (x1_f.equals(x1_s) && x2_f.equals(x2_s)) {
                row.setX6421("");
            }
        }

        if (!StringUtils.isEmpty(row.getX6521())) {
            String[] x6521Vector = row.getX6521().split("");
            String x1_s = x6521Vector[3];
            String x2_s = x6521Vector[2];

            if (x1_f.equals(x1_s) && x2_f.equals(x2_s)) {
                row.setX6521("");
            }

        }

        if (!StringUtils.isEmpty(row.getX54321())) {
            String[] x54321Vector = row.getX54321().split("");
            String x1_s = x54321Vector[4];
            String x2_s = x54321Vector[3];

            if (x1_f.equals(x1_s) && x2_f.equals(x2_s)) {
                row.setX54321("");
            }
        }

        if (!StringUtils.isEmpty(row.getX64321())) {
            String[] x64321Vector = row.getX64321().split("");
            String x1_s = x64321Vector[4];
            String x2_s = x64321Vector[3];

            if (x1_f.equals(x1_s) && x2_f.equals(x2_s)) {
                row.setX64321("");
            }
        }

        if (!StringUtils.isEmpty(row.getX65321())) {
            String[] x65321Vector = row.getX65321().split("");
            String x1_s = x65321Vector[4];
            String x2_s = x65321Vector[3];

            if (x1_f.equals(x1_s) && x2_f.equals(x2_s)) {
                row.setX65321("");
            }
        }
        if (!StringUtils.isEmpty(row.getX65421())) {
            String[] x65421Vector = row.getX65421().split("");
            String x1_s = x65421Vector[4];
            String x2_s = x65421Vector[3];

            if (x1_f.equals(x1_s) && x2_f.equals(x2_s)) {
                row.setX65421("");
            }
        }

        if (!StringUtils.isEmpty(row.getX654321())) {
            String[] x654321Vector = row.getX654321().split("");
            String x1_s = x654321Vector[5];
            String x2_s = x654321Vector[4];

            if (x1_f.equals(x1_s) && x2_f.equals(x2_s)) {
                row.setX654321("");
            }
        }
    }

    private void validateX31(Coefficient row) {
        if (StringUtils.isEmpty(row.getX31())) {
            return;
        }
        String[] x31Vector = row.getX31().split("");
        String x3_f = x31Vector[0];
        String x1_f = x31Vector[1];


        String[] x321Vector = row.getX321().split("");
        String x3_s = x321Vector[0];
        String x1_s = x321Vector[2];

        if (x3_f.equals(x3_s) && x1_f.equals(x1_s)) {
            row.setX321("");
        }

        String[] x431Vector = row.getX431().split("");
        x3_s = x431Vector[1];
        x1_s = x431Vector[2];

        if (x3_f.equals(x3_s) && x1_f.equals(x1_s)) {
            row.setX431("");
        }

        String[] x531Vector = row.getX531().split("");
        x3_s = x531Vector[1];
        x1_s = x531Vector[2];

        if (x3_f.equals(x3_s) && x1_f.equals(x1_s)) {
            row.setX531("");
        }

        String[] x631Vector = row.getX631().split("");
        x3_s = x631Vector[1];
        x1_s = x631Vector[2];

        if (x3_f.equals(x3_s) && x1_f.equals(x1_s)) {
            row.setX631("");
        }

        String[] x4321Vector = row.getX4321().split("");
        x3_s = x4321Vector[1];
        x1_s = x4321Vector[3];

        if (x3_f.equals(x3_s) && x1_f.equals(x1_s)) {
            row.setX4321("");
        }

        String[] x5321Vector = row.getX4321().split("");
        x3_s = x5321Vector[1];
        x1_s = x5321Vector[3];

        if (x3_f.equals(x3_s) && x1_f.equals(x1_s)) {
            row.setX5321("");
        }

        String[] x6321Vector = row.getX6321().split("");
        x3_s = x6321Vector[1];
        x1_s = x6321Vector[3];

        if (x3_f.equals(x3_s) && x1_f.equals(x1_s)) {
            row.setX6321("");
        }


        String[] x5431Vector = row.getX5431().split("");
        x3_s = x5431Vector[2];
        x1_s = x5431Vector[3];

        if (x3_f.equals(x3_s) && x1_f.equals(x1_s)) {
            row.setX5431("");
        }

        String[] x6431Vector = row.getX6431().split("");
        x3_s = x6431Vector[2];
        x1_s = x6431Vector[3];

        if (x3_f.equals(x3_s) && x1_f.equals(x1_s)) {
            row.setX6431("");
        }


        String[] x6531Vector = row.getX6531().split("");
        x3_s = x6531Vector[2];
        x1_s = x6531Vector[3];

        if (x3_f.equals(x3_s) && x1_f.equals(x1_s)) {
            row.setX6531("");
        }

        String[] x54321Vector = row.getX54321().split("");
        x3_s = x54321Vector[2];
        x1_s = x54321Vector[4];

        if (x3_f.equals(x3_s) && x1_f.equals(x1_s)) {
            row.setX54321("");
        }

        String[] x64321Vector = row.getX64321().split("");
        x3_s = x64321Vector[2];
        x1_s = x64321Vector[4];

        if (x3_f.equals(x3_s) && x1_f.equals(x1_s)) {
            row.setX64321("");
        }

        String[] x65321Vector = row.getX65321().split("");
        x3_s = x65321Vector[2];
        x1_s = x65321Vector[4];

        if (x3_f.equals(x3_s) && x1_f.equals(x1_s)) {
            row.setX65321("");
        }


        String[] x654321Vector = row.getX654321().split("");
        x3_s = x654321Vector[3];
        x1_s = x654321Vector[5];

        if (x3_f.equals(x3_s) && x1_f.equals(x1_s)) {
            row.setX654321("");
        }
    }

    private void validateX41(Coefficient row) {
        if (StringUtils.isEmpty(row.getX41())) {
            return;
        }
        String[] x41Vector = row.getX41().split("");
        String x4_f = x41Vector[0];
        String x1_f = x41Vector[1];

        String[] x421Vector = row.getX421().split("");
        String x4_s = x421Vector[0];
        String x1_s = x421Vector[2];

        if (x4_f.equals(x4_s) && x1_f.equals(x1_s)) {
            row.setX421("");
        }

        String[] x431Vector = row.getX431().split("");
        x4_s = x431Vector[0];
        x1_s = x431Vector[2];

        if (x4_f.equals(x4_s) && x1_f.equals(x1_s)) {
            row.setX431("");
        }

        String[] x541Vector = row.getX541().split("");
        x4_s = x541Vector[1];
        x1_s = x541Vector[2];

        if (x4_f.equals(x4_s) && x1_f.equals(x1_s)) {
            row.setX541("");
        }

        String[] x641Vector = row.getX641().split("");
        x4_s = x641Vector[1];
        x1_s = x641Vector[2];

        if (x4_f.equals(x4_s) && x1_f.equals(x1_s)) {
            row.setX641("");
        }


        String[] x4321Vector = row.getX4321().split("");
        x4_s = x4321Vector[0];
        x1_s = x4321Vector[3];

        if (x4_f.equals(x4_s) && x1_f.equals(x1_s)) {
            row.setX4321("");
        }


        String[] x5421Vector = row.getX5421().split("");
        x4_s = x5421Vector[1];
        x1_s = x5421Vector[3];

        if (x4_f.equals(x4_s) && x1_f.equals(x1_s)) {
            row.setX5421("");
        }

        String[] x6421Vector = row.getX6421().split("");
        x4_s = x6421Vector[1];
        x1_s = x6421Vector[3];

        if (x4_f.equals(x4_s) && x1_f.equals(x1_s)) {
            row.setX6421("");
        }

        String[] x5431Vector = row.getX5431().split("");
        x4_s = x5431Vector[1];
        x1_s = x5431Vector[3];

        if (x4_f.equals(x4_s) && x1_f.equals(x1_s)) {
            row.setX5431("");
        }

        String[] x6431Vector = row.getX6431().split("");
        x4_s = x6431Vector[1];
        x1_s = x6431Vector[3];

        if (x4_f.equals(x4_s) && x1_f.equals(x1_s)) {
            row.setX6431("");
        }


        String[] x54321Vector = row.getX54321().split("");
        x4_s = x54321Vector[1];
        x1_s = x54321Vector[4];

        if (x4_f.equals(x4_s) && x1_f.equals(x1_s)) {
            row.setX54321("");
        }

        String[] x64321Vector = row.getX64321().split("");
        x4_s = x64321Vector[1];
        x1_s = x64321Vector[4];

        if (x4_f.equals(x4_s) && x1_f.equals(x1_s)) {
            row.setX64321("");
        }

        String[] x654321Vector = row.getX654321().split("");
        x4_s = x654321Vector[2];
        x1_s = x654321Vector[5];

        if (x4_f.equals(x4_s) && x1_f.equals(x1_s)) {
            row.setX654321("");
        }
    }

    private void validateX321(Coefficient row) {
        if (StringUtils.isEmpty(row.getX321())) {
            return;
        }
        String[] x321Vector = row.getX321().split("");
        String x3_f = x321Vector[0];
        String x2_f = x321Vector[1];
        String x1_f = x321Vector[2];

        if (!StringUtils.isEmpty(row.getX4321())) {
            String[] x4321Vector = row.getX4321().split("");
            String x3_s = x4321Vector[1];
            String x2_s = x4321Vector[2];
            String x1_s = x4321Vector[3];

            if (x3_f.equals(x3_s) && x2_f.equals(x2_s) && x1_f.equals(x1_s)) {
                row.setX421("");
            }
        }
        if (StringUtils.isEmpty(row.getX5321())) {

            String[] x5321Vector = row.getX5321().split("");
            String x3_s = x5321Vector[1];
            String x2_s = x5321Vector[2];
            String x1_s = x5321Vector[3];

            if (x3_f.equals(x3_s) && x2_f.equals(x2_s) && x1_f.equals(x1_s)) {
                row.setX5321("");
            }
        }

        if (!StringUtils.isEmpty(row.getX6321())) {
            String[] x6321Vector = row.getX6321().split("");
            String x3_s = x6321Vector[1];
            String x2_s = x6321Vector[2];
            String x1_s = x6321Vector[3];

            if (x3_f.equals(x3_s) && x2_f.equals(x2_s) && x1_f.equals(x1_s)) {
                row.setX6321("");
            }
        }

        if (!StringUtils.isEmpty(row.getX54321())) {
            String[] x54321Vector = row.getX54321().split("");
            String x3_s = x54321Vector[2];
            String x2_s = x54321Vector[3];
            String x1_s = x54321Vector[4];

            if (x3_f.equals(x3_s) && x2_f.equals(x2_s) && x1_f.equals(x1_s)) {
                row.setX54321("");
            }
        }
        if (!StringUtils.isEmpty(row.getX64321())) {
            String[] x64321Vector = row.getX64321().split("");
            String x3_s = x64321Vector[2];
            String x2_s = x64321Vector[3];
            String x1_s = x64321Vector[4];

            if (x3_f.equals(x3_s) && x2_f.equals(x2_s) && x1_f.equals(x1_s)) {
                row.setX64321("");
            }
        }

        if (!StringUtils.isEmpty(row.getX65321())) {
            String[] x65321Vector = row.getX65321().split("");
            String x3_s = x65321Vector[2];
            String x2_s = x65321Vector[3];
            String x1_s = x65321Vector[4];

            if (x3_f.equals(x3_s) && x2_f.equals(x2_s) && x1_f.equals(x1_s)) {
                row.setX65321("");
            }
        }

        if (StringUtils.isEmpty(row.getX654321())) {
            String[] x654321Vector = row.getX654321().split("");
            String x3_s = x654321Vector[3];
            String x2_s = x654321Vector[4];
            String x1_s = x654321Vector[5];

            if (x3_f.equals(x3_s) && x2_f.equals(x2_s) && x1_f.equals(x1_s)) {
                row.setX654321("");
            }
        }
    }

    private void validateX631(Coefficient row) {
        if (StringUtils.isEmpty(row.getX631())) {
            return;
        }
        String[] x321Vector = row.getX631().split("");
        String x6_f = x321Vector[0];
        String x3_f = x321Vector[1];
        String x1_f = x321Vector[2];

        if (!StringUtils.isEmpty(row.getX6321())) {

            String[] x6321Vector = row.getX6321().split("");
            String x6_s = x6321Vector[0];
            String x3_s = x6321Vector[1];
            String x1_s = x6321Vector[3];

            if (x6_f.equals(x6_s) && x3_f.equals(x3_s) && x1_f.equals(x1_s)) {
                row.setX6321("");
            }
        }


        if (!StringUtils.isEmpty(row.getX6431())) {
            String[] x6431Vector = row.getX6431().split("");
            String x6_s = x6431Vector[0];
            String x3_s = x6431Vector[2];
            String x1_s = x6431Vector[3];

            if (x6_f.equals(x6_s) && x3_f.equals(x3_s) && x1_f.equals(x1_s)) {
                row.setX6431("");
            }
        }

        if (!StringUtils.isEmpty(row.getX6531())) {
            String[] x6531Vector = row.getX6531().split("");
            String x6_s = x6531Vector[0];
            String x3_s = x6531Vector[2];
            String x1_s = x6531Vector[3];

            if (x6_f.equals(x6_s) && x3_f.equals(x3_s) && x1_f.equals(x1_s)) {
                row.setX6531("");
            }
        }

        if (!StringUtils.isEmpty(row.getX64321())) {
            String[] x64321Vector = row.getX64321().split("");
            String x6_s = x64321Vector[0];
            String x3_s = x64321Vector[2];
            String x1_s = x64321Vector[4];

            if (x6_f.equals(x6_s) && x3_f.equals(x3_s) && x1_f.equals(x1_s)) {
                row.setX64321("");
            }
        }

        if (!StringUtils.isEmpty(row.getX65321())) {
            String[] x65321Vector = row.getX65321().split("");
            String x6_s = x65321Vector[0];
            String x3_s = x65321Vector[2];
            String x1_s = x65321Vector[4];

            if (x6_f.equals(x6_s) && x3_f.equals(x3_s) && x1_f.equals(x1_s)) {
                row.setX65321("");
            }

        }
        if (!StringUtils.isEmpty(row.getX654321())) {

            String[] x654321Vector = row.getX654321().split("");
            String x6_s = x654321Vector[0];
            String x3_s = x654321Vector[3];
            String x1_s = x654321Vector[5];

            if (x6_f.equals(x6_s) && x3_f.equals(x3_s) && x1_f.equals(x1_s)) {
                row.setX654321("");
            }
        }
    }

    private void validateX632(Coefficient row) {
        if (StringUtils.isEmpty(row.getX632())) {
            return;
        }
        String[] x632Vector = row.getX632().split("");
        String x6_f = x632Vector[0];
        String x3_f = x632Vector[1];
        String x2_f = x632Vector[2];

        if (!StringUtils.isEmpty(row.getX6321())) {
            String[] x6321Vector = row.getX6321().split("");
            String x6_s = x6321Vector[0];
            String x3_s = x6321Vector[1];
            String x2_s = x6321Vector[2];

            if (x6_f.equals(x6_s) && x3_f.equals(x3_s) && x2_f.equals(x2_s)) {
                row.setX6321("");
            }
        }

        if (!StringUtils.isEmpty(row.getX6432())) {
            String[] x6432Vector = row.getX6432().split("");
            String x6_s = x6432Vector[0];
            String x3_s = x6432Vector[2];
            String x2_s = x6432Vector[3];

            if (x6_f.equals(x6_s) && x3_f.equals(x3_s) && x2_f.equals(x2_s)) {
                row.setX6432("");
            }
        }

        if (!StringUtils.isEmpty(row.getX6532())) {
            String[] x6532Vector = row.getX6532().split("");
            String x6_s = x6532Vector[0];
            String x3_s = x6532Vector[2];
            String x2_s = x6532Vector[3];

            if (x6_f.equals(x6_s) && x3_f.equals(x3_s) && x2_f.equals(x2_s)) {
                row.setX6532("");
            }
        }

        if (!StringUtils.isEmpty(row.getX64321())) {
            String[] x64321Vector = row.getX64321().split("");
            String x6_s = x64321Vector[0];
            String x3_s = x64321Vector[2];
            String x2_s = x64321Vector[3];

            if (x6_f.equals(x6_s) && x3_f.equals(x3_s) && x2_f.equals(x2_s)) {
                row.setX64321("");
            }
        }

        if (!StringUtils.isEmpty(row.getX65321())) {
            String[] x65321Vector = row.getX65321().split("");
            String x6_s = x65321Vector[0];
            String x3_s = x65321Vector[2];
            String x2_s = x65321Vector[3];

            if (x6_f.equals(x6_s) && x3_f.equals(x3_s) && x2_f.equals(x2_s)) {
                row.setX65321("");
            }
        }

        if (!StringUtils.isEmpty(row.getX65432())) {
            String[] x65432Vector = row.getX65432().split("");
            String x6_s = x65432Vector[0];
            String x3_s = x65432Vector[3];
            String x2_s = x65432Vector[4];

            if (x6_f.equals(x6_s) && x3_f.equals(x3_s) && x2_f.equals(x2_s)) {
                row.setX65432("");
            }
        }

        if (!StringUtils.isEmpty(row.getX654321())) {
            String[] x654321Vector = row.getX654321().split("");
            String x6_s = x654321Vector[0];
            String x3_s = x654321Vector[3];
            String x2_s = x654321Vector[4];

            if (x6_f.equals(x6_s) && x3_f.equals(x3_s) && x2_f.equals(x2_s)) {
                row.setX654321("");
            }
        }
    }

    private void validateX4321(Coefficient row) {

        if (StringUtils.isEmpty(row.getX4321())) {
            return;
        }
        String[] x4321Vector = row.getX4321().split("");
        String x4_f = x4321Vector[0];
        String x3_f = x4321Vector[1];
        String x2_f = x4321Vector[2];
        String x1_f = x4321Vector[3];


        if (!StringUtils.isEmpty(row.getX54321())) {
            String[] x54321Vector = row.getX54321().split("");
            String x4_s = x54321Vector[1];
            String x3_s = x54321Vector[2];
            String x2_s = x54321Vector[3];
            String x1_s = x54321Vector[4];

            if (x4_f.equals(x4_s) && x3_f.equals(x3_s) && x2_f.equals(x2_s) && x1_f.equals(x1_s)) {
                row.setX54321("");
            }
        }

        if (!StringUtils.isEmpty(row.getX64321())) {
            String[] x64321Vector = row.getX64321().split("");
            String x4_s = x64321Vector[1];
            String x3_s = x64321Vector[2];
            String x2_s = x64321Vector[3];
            String x1_s = x64321Vector[4];

            if (x4_f.equals(x4_s) && x3_f.equals(x3_s) && x2_f.equals(x2_s) && x1_f.equals(x1_s)) {
                row.setX64321("");
            }
        }

        if (!StringUtils.isEmpty(row.getX654321())) {
            String[] x654321Vector = row.getX654321().split("");
            String x4_s = x654321Vector[2];
            String x3_s = x654321Vector[3];
            String x2_s = x654321Vector[4];
            String x1_s = x654321Vector[5];

            if (x4_f.equals(x4_s) && x3_f.equals(x3_s) && x2_f.equals(x2_s) && x1_f.equals(x1_s)) {
                row.setX654321("");
            }
        }
    }

    private void validateX5321(Coefficient row) {
        if (StringUtils.isEmpty(row.getX5321())) {
            return;
        }
        String[] x5321Vector = row.getX5321().split("");
        String x5_f = x5321Vector[0];
        String x3_f = x5321Vector[1];
        String x2_f = x5321Vector[2];
        String x1_f = x5321Vector[3];

        if (!StringUtils.isEmpty(row.getX54321())) {
            String[] x54321Vector = row.getX54321().split("");
            String x5_s = x54321Vector[0];
            String x3_s = x54321Vector[2];
            String x2_s = x54321Vector[3];
            String x1_s = x54321Vector[4];

            if (x5_f.equals(x5_s) && x3_f.equals(x3_s) && x2_f.equals(x2_s) && x1_f.equals(x1_s)) {
                row.setX64321("");
            }
        }


        if (!StringUtils.isEmpty(row.getX65321())) {
            String[] x65321Vector = row.getX65321().split("");
            String x5_s = x65321Vector[1];
            String x3_s = x65321Vector[2];
            String x2_s = x65321Vector[3];
            String x1_s = x65321Vector[4];

            if (x5_f.equals(x5_s) && x3_f.equals(x3_s) && x2_f.equals(x2_s) && x1_f.equals(x1_s)) {
                row.setX65321("");
            }
        }

        if (!StringUtils.isEmpty(row.getX654321())) {
            String[] x654321Vector = row.getX654321().split("");
            String x5_s = x654321Vector[1];
            String x3_s = x654321Vector[3];
            String x2_s = x654321Vector[4];
            String x1_s = x654321Vector[5];

            if (x5_f.equals(x5_s) && x3_f.equals(x3_s) && x2_f.equals(x2_s) && x1_f.equals(x1_s)) {
                row.setX654321("");
            }
        }
    }

    private void validateX6321(Coefficient row) {
        if (StringUtils.isEmpty(row.getX6321())) {
            return;
        }
        String[] x6321Vector = row.getX6321().split("");
        String x6_f = x6321Vector[0];
        String x3_f = x6321Vector[1];
        String x2_f = x6321Vector[2];
        String x1_f = x6321Vector[3];

        if (!StringUtils.isEmpty(row.getX64321())) {
            String[] x64321Vector = row.getX64321().split("");
            String x6_s = x64321Vector[0];
            String x3_s = x64321Vector[2];
            String x2_s = x64321Vector[3];
            String x1_s = x64321Vector[4];

            if (x6_f.equals(x6_s) && x3_f.equals(x3_s) && x2_f.equals(x2_s) && x1_f.equals(x1_s)) {
                row.setX64321("");
            }
        }

        if (!StringUtils.isEmpty(row.getX65321())) {
            String[] x65321Vector = row.getX65321().split("");
            String x6_s = x65321Vector[1];
            String x3_s = x65321Vector[2];
            String x2_s = x65321Vector[3];
            String x1_s = x65321Vector[4];

            if (x6_f.equals(x6_s) && x3_f.equals(x3_s) && x2_f.equals(x2_s) && x1_f.equals(x1_s)) {
                row.setX65321("");
            }
        }

        if (!StringUtils.isEmpty(row.getX654321())) {
            String[] x654321Vector = row.getX654321().split("");
            String x6_s = x654321Vector[0];
            String x3_s = x654321Vector[3];
            String x2_s = x654321Vector[4];
            String x1_s = x654321Vector[5];

            if (x6_f.equals(x6_s) && x3_f.equals(x3_s) && x2_f.equals(x2_s) && x1_f.equals(x1_s)) {
                row.setX654321("");
            }
        }
    }

    private void validateX6521(Coefficient row) {
        if (StringUtils.isEmpty(row.getX6521())) {
            return;
        }
        String[] x6521Vector = row.getX6521().split("");
        String x6_f = x6521Vector[0];
        String x5_f = x6521Vector[1];
        String x2_f = x6521Vector[2];
        String x1_f = x6521Vector[3];

        if (!StringUtils.isEmpty(row.getX65321())) {
            String[] x65321Vector = row.getX65321().split("");
            String x6_s = x65321Vector[0];
            String x5_s = x65321Vector[1];
            String x2_s = x65321Vector[3];
            String x1_s = x65321Vector[4];

            if (x6_f.equals(x6_s) && x5_f.equals(x5_s) && x2_f.equals(x2_s) && x1_f.equals(x1_s)) {
                row.setX65321("");
            }
        }

        if (!StringUtils.isEmpty(row.getX65421())) {
            String[] x65421Vector = row.getX65421().split("");
            String x6_s = x65421Vector[0];
            String x5_s = x65421Vector[1];
            String x2_s = x65421Vector[3];
            String x1_s = x65421Vector[4];

            if (x6_f.equals(x6_s) && x5_f.equals(x5_s) && x2_f.equals(x2_s) && x1_f.equals(x1_s)) {
                row.setX65421("");
            }
        }

        if (!StringUtils.isEmpty(row.getX654321())) {
            String[] x654321Vector = row.getX654321().split("");
            String x6_s = x654321Vector[0];
            String x5_s = x654321Vector[1];
            String x2_s = x654321Vector[4];
            String x1_s = x654321Vector[5];

            if (x6_f.equals(x6_s) && x5_f.equals(x5_s) && x2_f.equals(x2_s) && x1_f.equals(x1_s)) {
                row.setX654321("");
            }
        }
    }

    private void validateX5431(Coefficient row) {
        if (StringUtils.isEmpty(row.getX5431())) {
            return;
        }
        String[] x5431Vector = row.getX5431().split("");
        String x5_f = x5431Vector[0];
        String x4_f = x5431Vector[1];
        String x3_f = x5431Vector[2];
        String x1_f = x5431Vector[3];

        if (!StringUtils.isEmpty(row.getX54321())) {
            String[] x54321Vector = row.getX54321().split("");
            String x5_s = x54321Vector[0];
            String x4_s = x54321Vector[1];
            String x3_s = x54321Vector[2];
            String x1_s = x54321Vector[4];

            if (x5_f.equals(x5_s) && x4_f.equals(x4_s) && x3_f.equals(x3_s) && x1_f.equals(x1_s)) {
                row.setX54321("");
            }
        }


        if (!StringUtils.isEmpty(row.getX654321())) {
            String[] x654321Vector = row.getX654321().split("");
            String x5_s = x654321Vector[1];
            String x4_s = x654321Vector[2];
            String x3_s = x654321Vector[3];
            String x1_s = x654321Vector[5];

            if (x5_f.equals(x5_s) && x4_f.equals(x4_s) && x3_f.equals(x3_s) && x1_f.equals(x1_s)) {
                row.setX654321("");
            }
        }
    }

    private void validateX5432(Coefficient row) {
        if (StringUtils.isEmpty(row.getX5432())) {
            return;
        }
        String[] x5432Vector = row.getX5432().split("");
        String x5_f = x5432Vector[0];
        String x4_f = x5432Vector[1];
        String x3_f = x5432Vector[2];
        String x2_f = x5432Vector[3];

        if (!StringUtils.isEmpty(row.getX54321())) {
            String[] x54321Vector = row.getX54321().split("");
            String x5_s = x54321Vector[0];
            String x4_s = x54321Vector[1];
            String x3_s = x54321Vector[2];
            String x2_s = x54321Vector[3];

            if (x5_f.equals(x5_s) && x4_f.equals(x4_s) && x3_f.equals(x3_s) && x2_f.equals(x2_s)) {
                row.setX54321("");
            }
        }

        if (!StringUtils.isEmpty(row.getX65432())) {
            String[] x65432Vector = row.getX65432().split("");
            String x5_s = x65432Vector[1];
            String x4_s = x65432Vector[2];
            String x3_s = x65432Vector[3];
            String x2_s = x65432Vector[4];

            if (x5_f.equals(x5_s) && x4_f.equals(x4_s) && x3_f.equals(x3_s) && x2_f.equals(x2_s)) {
                row.setX65432("");
            }
        }

        if (!StringUtils.isEmpty(row.getX654321())) {
            String[] x654321Vector = row.getX654321().split("");
            String x5_s = x654321Vector[1];
            String x4_s = x654321Vector[2];
            String x3_s = x654321Vector[3];
            String x2_s = x654321Vector[4];

            if (x5_f.equals(x5_s) && x4_f.equals(x4_s) && x3_f.equals(x3_s) && x2_f.equals(x2_s)) {
                row.setX654321("");
            }
        }
    }

    private void validateX6432(Coefficient row) {
        if (StringUtils.isEmpty(row.getX6432())) {
            return;
        }
        String[] x6432Vector = row.getX6432().split("");
        String x6_f = x6432Vector[0];
        String x4_f = x6432Vector[1];
        String x3_f = x6432Vector[2];
        String x2_f = x6432Vector[3];

        if (!StringUtils.isEmpty(row.getX64321())) {
            String[] x64321Vector = row.getX64321().split("");
            String x6_s = x64321Vector[0];
            String x4_s = x64321Vector[1];
            String x3_s = x64321Vector[2];
            String x2_s = x64321Vector[3];

            if (x6_f.equals(x6_s) && x4_f.equals(x4_s) && x3_f.equals(x3_s) && x2_f.equals(x2_s)) {
                row.setX64321("");
            }
        }

        if (!StringUtils.isEmpty(row.getX65432())) {
            String[] x65432Vector = row.getX65432().split("");
            String x6_s = x65432Vector[0];
            String x4_s = x65432Vector[2];
            String x3_s = x65432Vector[3];
            String x2_s = x65432Vector[4];

            if (x6_f.equals(x6_s) && x4_f.equals(x4_s) && x3_f.equals(x3_s) && x2_f.equals(x2_s)) {
                row.setX65432("");
            }
        }

        if (!StringUtils.isEmpty(row.getX654321())) {
            String[] x654321Vector = row.getX654321().split("");
            String x6_s = x654321Vector[0];
            String x4_s = x654321Vector[2];
            String x3_s = x654321Vector[3];
            String x2_s = x654321Vector[4];

            if (x6_f.equals(x6_s) && x4_f.equals(x4_s) && x3_f.equals(x3_s) && x2_f.equals(x2_s)) {
                row.setX654321("");
            }
        }

    }
}
